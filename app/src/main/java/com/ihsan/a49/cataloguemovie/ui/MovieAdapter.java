package com.ihsan.a49.cataloguemovie.ui;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ihsan.a49.cataloguemovie.BuildConfig;
import com.ihsan.a49.cataloguemovie.R;
import com.ihsan.a49.cataloguemovie.model.ModelData;
import com.ihsan.a49.cataloguemovie.ui.detail.DetailMovie;
import com.ihsan.a49.cataloguemovie.util.FormatDate;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.HolderData> {

    private static final String KEY_MOVIE = "idMovie";
    private final List<ModelData> mLists;


    public MovieAdapter(List<ModelData> mLists) {
        this.mLists = mLists;
    }

    @Override
    public void onBindViewHolder(@NonNull final HolderData holder, int position) {

        final ModelData md = mLists.get(position);

        String date = FormatDate.loadDate(md.getRelease_date());

        holder.title.setText(md.getTitle());
        holder.release.setText(date);
        final String mId = md.getId();
        Glide.with(holder.itemView.getContext())
                .load(BuildConfig.IMAGE_URL + md.getPoster_path())
                .into(holder.poster);
        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(holder.itemView.getContext(), DetailMovie.class);
            intent.putExtra(KEY_MOVIE, mId);
            holder.itemView.getContext().startActivity(intent);
        });
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list, parent, false);
        return new HolderData(layout);
    }

    @Override
    public int getItemCount() {
        if (mLists == null) {
            return 0;
        } else {
            return mLists.size();
        }
    }

    static class HolderData extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        TextView title;
        @BindView(R.id.tv_release)
        TextView release;
        @BindView(R.id.img_poster)
        ImageView poster;

        HolderData(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }


}
