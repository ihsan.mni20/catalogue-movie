package com.ihsan.a49.cataloguemovie.ui.search;

import com.ihsan.a49.cataloguemovie.model.ModelData;

import java.util.List;

public class SearchView {
    interface View {
        void setViewSearchMovie(List<ModelData> model);
    }


    interface Presenter {
        void loadDataSearch();

        void start();
    }
}
