package com.ihsan.a49.cataloguemovie.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.ihsan.a49.cataloguemovie.data.database.DatabaseContract;

import java.util.List;

import static android.provider.BaseColumns._ID;
import static com.ihsan.a49.cataloguemovie.data.database.DatabaseContract.getColumnString;

public class ModelDetail implements Parcelable {

    public static final Creator<ModelDetail> CREATOR = new Creator<ModelDetail>() {
        @Override
        public ModelDetail createFromParcel(Parcel in) {
            return new ModelDetail(in);
        }

        @Override
        public ModelDetail[] newArray(int size) {
            return new ModelDetail[size];
        }
    };
    private List<ModelGenres> genres;

    public ModelDetail(String poster_path, List<ModelGenres> genres) {
        this.poster_path = poster_path;
        this.genres = genres;
    }

    private String title, poster_path, overview, release_date, vote_average, id, backdrop_path, vote_count, runtime, stringGenre;

    public ModelDetail() {

    }

    protected ModelDetail(Parcel in) {
        title = in.readString();
        poster_path = in.readString();
        overview = in.readString();
        release_date = in.readString();
        vote_average = in.readString();
        id = in.readString();
        backdrop_path = in.readString();
        vote_count = in.readString();
        runtime = in.readString();
        stringGenre = in.readString();
    }

    public ModelDetail(Cursor cursor) {
        this.title = getColumnString(cursor, DatabaseContract.MovieColumns.TITLE);
        this.poster_path = getColumnString(cursor, DatabaseContract.MovieColumns.POSTER_PATH);
        this.overview = getColumnString(cursor, DatabaseContract.MovieColumns.OVERVIEW);
        this.release_date = getColumnString(cursor, DatabaseContract.MovieColumns.RELEASE_DATE);
        this.vote_average = getColumnString(cursor, DatabaseContract.MovieColumns.VOTE_AVERAGE);
        this.backdrop_path = getColumnString(cursor, DatabaseContract.MovieColumns.BACKDROP_PATH);
        this.vote_count = getColumnString(cursor, DatabaseContract.MovieColumns.VOTE_COUNT);
        this.runtime = getColumnString(cursor, DatabaseContract.MovieColumns.RUNTIME);
        this.stringGenre = getColumnString(cursor, DatabaseContract.MovieColumns.GENRE);
        this.id = getColumnString(cursor, _ID);
    }

    public String getStringGenre() {
        return stringGenre;
    }

    public void setStringGenre(String stringGenre) {
        this.stringGenre = stringGenre;
    }

    public void setGenres(List<ModelGenres> genres) {
        this.genres = genres;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public void setVote_average(String vote_average) {
        this.vote_average = vote_average;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public String getVote_count() {
        return vote_count;
    }

    public void setVote_count(String vote_count) {
        this.vote_count = vote_count;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getRelease_date() {
        return release_date;
    }

    public String getVote_average() {
        return vote_average;
    }

    public List<ModelGenres> getGenres() {
        return genres;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(poster_path);
        parcel.writeString(overview);
        parcel.writeString(release_date);
        parcel.writeString(vote_average);
        parcel.writeString(id);
        parcel.writeString(backdrop_path);
        parcel.writeString(vote_count);
        parcel.writeString(runtime);
        parcel.writeString(stringGenre);
    }
}


