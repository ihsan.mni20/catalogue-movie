package com.ihsan.a49.cataloguemovie.ui.playing;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ihsan.a49.cataloguemovie.R;
import com.ihsan.a49.cataloguemovie.model.ModelData;
import com.ihsan.a49.cataloguemovie.ui.MovieAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class NowPlayingFragment extends Fragment implements NowPlayingView.View {
    @BindView(R.id.recycle_now)
    RecyclerView mRecycler;
    private static final String STATE_ITEMS = "items";
    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;
    ArrayList<ModelData> mItems;
    MovieAdapter mAdapter;
    NowPlayingPresenter nowPlayingPresenter;
    private Unbinder unBinder;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View V = inflater.inflate(R.layout.fragment_now_playing, container, false);

        unBinder = ButterKnife.bind(this, V);

        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

        nowPlayingPresenter = new NowPlayingPresenter(this);

        return V;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            progress_bar.setVisibility(View.GONE);

            mItems = savedInstanceState.getParcelableArrayList(STATE_ITEMS);
            mAdapter = new MovieAdapter(mItems);
            mRecycler.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();

        } else {

            nowPlayingPresenter.start();

        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unBinder.unbind();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {

        outState.putParcelableArrayList(STATE_ITEMS, mItems);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void setViewNowPlayingMovie(List<ModelData> model) {
        progress_bar.setVisibility(View.GONE);

        mItems = (ArrayList<ModelData>) model;

        mAdapter = new MovieAdapter(model);
        mRecycler.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }
}
