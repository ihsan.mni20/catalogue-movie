package com.ihsan.a49.cataloguemovie.data.api;

import com.ihsan.a49.cataloguemovie.model.ModelDetail;
import com.ihsan.a49.cataloguemovie.model.ModelResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiRequestData {

    @GET("movie/now_playing")
    Observable<ModelResponse> getNowPlaying();

    @GET("movie/upcoming")
    Observable<ModelResponse> getUpComing();

    @GET("search/movie")
    Observable<ModelResponse> getSearch(@Query("query") String query);

    @GET("movie/{movie_id}")
    Observable<ModelDetail> getDetail(@Path("movie_id") String movie_id);

}
