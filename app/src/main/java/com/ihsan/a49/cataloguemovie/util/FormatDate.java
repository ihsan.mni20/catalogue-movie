package com.ihsan.a49.cataloguemovie.util;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatDate {

    public static String loadDate(String date) {

        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("EEEE, MMM dd, yyyy");
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Date Date = null;
        try {
            Date = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return sdf.format(Date);
    }
}
