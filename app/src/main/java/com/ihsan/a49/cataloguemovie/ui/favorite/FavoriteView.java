package com.ihsan.a49.cataloguemovie.ui.favorite;

import com.ihsan.a49.cataloguemovie.model.ModelDetail;

import java.util.List;

import io.reactivex.Observable;

public class FavoriteView {
    interface View {
        void setViewFavoriteMovie(List<ModelDetail> model);
    }


    interface Presenter {
        void loadDataFavorite();

        Observable<List<ModelDetail>> fetchFavoriteMovies();

        void start();
    }
}
