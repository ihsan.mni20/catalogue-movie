package com.ihsan.a49.cataloguemovie.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.ihsan.a49.cataloguemovie.R;
import com.ihsan.a49.cataloguemovie.model.ModelDetail;

import java.util.Objects;
import java.util.concurrent.ExecutionException;

import static com.ihsan.a49.cataloguemovie.data.database.DatabaseContract.CONTENT_URI;


class StackRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {

    private Cursor mWidgetItems;
    private final Context mContext;

    StackRemoteViewsFactory(Context context, Intent intent) {
        mContext = context;
        int mAppWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
    }

    public void onCreate() {

    }


    @Override
    public void onDataSetChanged() {

        final long identityToken = Binder.clearCallingIdentity();

        mWidgetItems = mContext.getContentResolver().query(CONTENT_URI, null, null, null, null, null);

        Binder.restoreCallingIdentity(identityToken);
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        return mWidgetItems.getCount();
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.widget_item);
        ModelDetail modelDetail = getItem(position);

        Bitmap bitmap = null;

        try {
            bitmap = Glide.with(mContext)
                    .asBitmap()
                    .load("http://image.tmdb.org/t/p/w185" + modelDetail.getPoster_path())
                    .submit(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.d("Widget Load Error", Objects.requireNonNull(e.getMessage()));
        }

        rv.setImageViewBitmap(R.id.imageView, bitmap);

        Bundle extras = new Bundle();
        extras.putInt(FavoriteWidget.EXTRA_ITEM, position);
        Intent fillInIntent = new Intent();
        fillInIntent.putExtras(extras);

        rv.setOnClickFillInIntent(R.id.imageView, fillInIntent);
        return rv;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    private ModelDetail getItem(int position) {

        if (!mWidgetItems.moveToPosition(position)) {
            throw new IllegalStateException("Position invalid");

        }
        return new ModelDetail(mWidgetItems);
    }


}
