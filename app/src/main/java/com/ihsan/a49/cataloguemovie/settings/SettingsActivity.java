package com.ihsan.a49.cataloguemovie.settings;


import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import android.provider.Settings;
import android.view.MenuItem;
import android.widget.Toast;

import com.ihsan.a49.cataloguemovie.R;
import com.ihsan.a49.cataloguemovie.reminder.ReleaseToday;
import com.ihsan.a49.cataloguemovie.reminder.ReminderDaily;
import com.ihsan.a49.cataloguemovie.ui.MainActivity;

import static android.widget.Toast.LENGTH_SHORT;

public class SettingsActivity extends AppCompatPreferenceActivity {

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getFragmentManager().beginTransaction().replace(android.R.id.content, new MainPreferenceFragment()).commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent mItem = new Intent(this, MainActivity.class);
            startActivity(mItem);
            return true;
        }
        return super.onOptionsItemSelected(item);


    }

    public static class MainPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_main);


            Preference local = findPreference(getString(R.string.key_local));

            SwitchPreference reminderDaily = (SwitchPreference) findPreference(getString(R.string.key_reminder_daily));
            SwitchPreference reminderNowPlaying = (SwitchPreference) findPreference(getString(R.string.key_reminder_nowplaying));

            local.setOnPreferenceClickListener(preference -> {
                Intent mItem = new Intent(Settings.ACTION_LOCALE_SETTINGS);
                startActivity(mItem);
                return false;
            });

            reminderDaily.setOnPreferenceChangeListener((preference, newValue) -> {
                if ((Boolean) newValue) {
                    ReminderDaily.setReminder(getActivity(), getString(R.string.msg_daily_reminder), 7, 0);
                    Toast.makeText(getActivity(), "daily reminder On", LENGTH_SHORT).show();
                } else {
                    ReminderDaily.cancelReminder(getActivity());
                    Toast.makeText(getActivity(), "daily reminder Off", LENGTH_SHORT).show();
                }
                return true;
            });

            reminderNowPlaying.setOnPreferenceChangeListener((preference, newValue) -> {
                if ((Boolean) newValue) {
                    ReleaseToday.setReminder(getActivity(), 8, 0);
                    Toast.makeText(getActivity(), "Release reminder On", LENGTH_SHORT).show();
                } else {
                    ReleaseToday.cancelReminder(getActivity());
                    Toast.makeText(getActivity(), "Release reminder Off", LENGTH_SHORT).show();
                }
                return true;
            });


        }
    }

}
