package com.ihsan.a49.cataloguemovie.data.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import static android.provider.BaseColumns._ID;
import static com.ihsan.a49.cataloguemovie.data.database.DatabaseContract.TABLE_FAVORITE;

public class FavoriteHelper {

    public static final String DATABASE_TABLE = TABLE_FAVORITE;
    private final Context context;
    private DatabaseHelper databaseHelper;


    private SQLiteDatabase database;

    public FavoriteHelper(Context context) {

        this.context = context;
    }

    public void open() throws SQLException {
        databaseHelper = new DatabaseHelper(context);
        database = databaseHelper.getWritableDatabase();
    }

    public void close() {
        databaseHelper.close();
    }

    public boolean checkFavorite(String id) {
        boolean isFavorite;

        SQLiteDatabase readData = databaseHelper.getReadableDatabase();
        @SuppressLint("Recycle") Cursor cursor = readData.rawQuery("SELECT * FROM " + DATABASE_TABLE + " WHERE " + _ID + " = ?", new String[]{id});
        Log.d("RETRO", "cursor : " + cursor.getCount());
        if (cursor.getCount() == 0) {
            isFavorite = false;
        } else {
            isFavorite = true;
        }
        return isFavorite;
    }

    public Cursor queryByIdProvider(String id) {
        return database.query(DATABASE_TABLE, null
                , _ID + " = ?"
                , new String[]{id}
                , null
                , null
                , null
                , null);
    }

    public Cursor queryProvider() {
        return database.query(DATABASE_TABLE,
                null
                , null
                , null
                , null
                , null
                , _ID + " DESC");
    }

    public long insertProvider(ContentValues values) {
        return database.insert(DATABASE_TABLE, null, values);
    }

    public int updateProvider(String id, ContentValues values) {
        return database.update(DATABASE_TABLE, values, _ID + " = ?", new String[]{id});
    }

    public int deleteProvider(String id) {
        return database.delete(DATABASE_TABLE, _ID + " = ?", new String[]{id});
    }
}
