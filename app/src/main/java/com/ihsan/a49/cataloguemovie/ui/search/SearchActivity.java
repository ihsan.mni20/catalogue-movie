package com.ihsan.a49.cataloguemovie.ui.search;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ihsan.a49.cataloguemovie.R;
import com.ihsan.a49.cataloguemovie.model.ModelData;
import com.ihsan.a49.cataloguemovie.ui.MovieAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends AppCompatActivity implements SearchView.View {
    private String search;
    @BindView(R.id.recycle_search)
    RecyclerView mRecycler;
    private static final String STATE_ITEMS = "items";
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    private static final String ARG_SEARCH = "search";
    SearchPresenter searchPresenter;

    MovieAdapter mAdapter;
    ArrayList<ModelData> mItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        mRecycler.setLayoutManager(new LinearLayoutManager(this));

        search = getIntent().getStringExtra(ARG_SEARCH);

        searchPresenter = new SearchPresenter(this, search);

        if (savedInstanceState != null) {

            mItems = savedInstanceState.getParcelableArrayList(STATE_ITEMS);
            mAdapter = new MovieAdapter(mItems);
            mRecycler.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();

        } else {
            searchPresenter.start();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelableArrayList(STATE_ITEMS, mItems);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void setViewSearchMovie(List<ModelData> model) {
        progressBar.setVisibility(View.GONE);
        mItems = (ArrayList<ModelData>) model;
        mAdapter = new MovieAdapter(model);
        mRecycler.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

}
