package com.ihsan.a49.cataloguemovie.data.api;

import com.ihsan.a49.cataloguemovie.BuildConfig;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class MovieServer {
    private static final String baseUrl = BuildConfig.BASE_URL;

    private static Retrofit retrofit;

    public static ApiRequestData getClient() {

        if (retrofit == null) {

            final ApiParameterInterceptor apiParameterInterceptor = new ApiParameterInterceptor();

            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .addInterceptor(apiParameterInterceptor)
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }

        return retrofit.create(ApiRequestData.class);
    }

}
