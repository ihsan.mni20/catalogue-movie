package com.ihsan.a49.cataloguemovie.reminder;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.ihsan.a49.cataloguemovie.R;
import com.ihsan.a49.cataloguemovie.data.api.ApiRequestData;
import com.ihsan.a49.cataloguemovie.data.api.MovieServer;
import com.ihsan.a49.cataloguemovie.model.ModelData;
import com.ihsan.a49.cataloguemovie.model.ModelResponse;
import com.ihsan.a49.cataloguemovie.ui.MainActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.app.PendingIntent.FLAG_ONE_SHOT;

public class ReleaseToday extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        //Log.d("RETRO", "onReceive");
        loadMovie(context);

    }

    public static void showNotification(Context context, Class<?> cls, String title, String content) {
        // Log.d("RETRO", "show notification ::" + title + ":::" + content);
        Random random = new Random();
        int m = random.nextInt(9999 - 1000);

        Intent notifyIntent = new Intent(context, cls);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

        PendingIntent pendingIntent = PendingIntent.getActivity(
                context,
                0,
                notifyIntent,
                FLAG_ONE_SHOT);

        builder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentIntent(pendingIntent)
                .setContentText(content)
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
                .setContentInfo("Info");

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(m, builder.build());

    }

    public static void setReminder(Context context, int hour, int min) {
        // Log.d("RETRO", "set reminder");

        //Log.d("RETRO", "Date " + getCurrentDate());
        Calendar setCalendar = Calendar.getInstance();

        Intent intent = new Intent(context, ReleaseToday.class);

        setCalendar.set(Calendar.HOUR_OF_DAY, hour);
        setCalendar.set(Calendar.MINUTE, min);
        setCalendar.set(Calendar.SECOND, 0);


        ComponentName receiver = new ComponentName(context, ReleaseToday.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);


        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                1, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        assert am != null;
        am.setInexactRepeating(AlarmManager.RTC_WAKEUP, setCalendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);

    }

    public static void cancelReminder(Context context) {

        ComponentName receiver = new ComponentName(context, ReleaseToday.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);

        Intent intent = new Intent(context, ReleaseToday.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                1, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        am.cancel(pendingIntent);
        pendingIntent.cancel();

    }

    public static String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat mdFormat = new SimpleDateFormat("yyyy-MM-dd");
        String st = mdFormat.format(c.getTime());
        return st;
    }

    @SuppressLint("CheckResult")
    private void loadMovie(final Context context) {
        ApiRequestData api = MovieServer.getClient();
        Observable<ModelResponse> getData;

        getData = api.getUpComing();

        getData.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(movie -> {
                            int index;
                            List<ModelData> item = movie.getResults();
                            int size = item.size();

                            for (index = 0; index < size; index++) {

                                if (item.get(index).getRelease_date().equals(getCurrentDate())) {

                                    String title = item.get(index).getTitle();
                                    String message = title + " rilis hari ini";
                                    showNotification(context, MainActivity.class, title, message);
                                }
                            }
                        }

                        , it -> Log.d("Load Server ", it.toString()));

    }
}
