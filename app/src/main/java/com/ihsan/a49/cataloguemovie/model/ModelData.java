package com.ihsan.a49.cataloguemovie.model;


import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.ihsan.a49.cataloguemovie.data.database.DatabaseContract;

import static android.provider.BaseColumns._ID;
import static com.ihsan.a49.cataloguemovie.data.database.DatabaseContract.getColumnString;

public class ModelData implements Parcelable {

    private final String release_date;
    private final String poster_path;
    private String id;
    private String title;
    private String overview;

    public ModelData(String id, String title, String overview, String release_date, String poster_path) {
        this.id = id;
        this.title = title;
        this.overview = overview;
        this.release_date = release_date;
        this.poster_path = poster_path;
    }

    public ModelData(Cursor cursor) {
        this.title = getColumnString(cursor, DatabaseContract.MovieColumns.TITLE);
        this.poster_path = getColumnString(cursor, DatabaseContract.MovieColumns.POSTER_PATH);
        this.overview = getColumnString(cursor, DatabaseContract.MovieColumns.OVERVIEW);
        this.release_date = getColumnString(cursor, DatabaseContract.MovieColumns.RELEASE_DATE);
        this.id = getColumnString(cursor, _ID);
    }

    public ModelData(Parcel in) {

        id = in.readString();
        title = in.readString();
        overview = in.readString();
        release_date = in.readString();
        poster_path = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getRelease_date() {
        return release_date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(overview);
        dest.writeString(release_date);
        dest.writeString(poster_path);

    }

    public static final Parcelable.Creator<ModelData> CREATOR = new Parcelable.Creator<ModelData>() {
        public ModelData createFromParcel(Parcel in) {
            return new ModelData(in);
        }

        public ModelData[] newArray(int size) {
            return new ModelData[size];
        }
    };
}
