package com.ihsan.a49.cataloguemovie.ui.detail;

import com.ihsan.a49.cataloguemovie.model.ModelDetail;

public class DetailView {
    interface View {
        void setViewDetailMovie(ModelDetail model);

        void setFavoriteIcon(boolean isFav);
    }


    interface Presenter {

        void loadDataDetail();

        void loadDataDetailFav();

        void start();
    }
}
