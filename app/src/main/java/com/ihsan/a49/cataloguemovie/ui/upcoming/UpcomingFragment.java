package com.ihsan.a49.cataloguemovie.ui.upcoming;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ihsan.a49.cataloguemovie.R;
import com.ihsan.a49.cataloguemovie.model.ModelData;
import com.ihsan.a49.cataloguemovie.ui.MovieAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpcomingFragment extends Fragment implements UpcomingView.View {

    @BindView(R.id.recycle_upcoming)
    RecyclerView mRecycler;
    private static final String STATE_ITEMS = "items";
    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;
    ArrayList<ModelData> mItems;
    MovieAdapter mAdapter;
    private Unbinder unBinder;
    private UpcomingPresenter upcomingPresenter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.fragment_upcoming, container, false);

        unBinder = ButterKnife.bind(this, V);
        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        upcomingPresenter = new UpcomingPresenter(this);

        return V;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {

            progress_bar.setVisibility(View.GONE);
            mItems = savedInstanceState.getParcelableArrayList(STATE_ITEMS);
            mAdapter = new MovieAdapter(mItems);
            mRecycler.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();

        } else {
            //Log.d("Save ", "server ");
            upcomingPresenter.start();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unBinder.unbind();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelableArrayList(STATE_ITEMS, mItems);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void setViewUpcomingMovie(List<ModelData> model) {
        progress_bar.setVisibility(View.GONE);

        mItems = (ArrayList<ModelData>) model;

        mAdapter = new MovieAdapter(model);
        mRecycler.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

}
