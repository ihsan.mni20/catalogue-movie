package com.ihsan.a49.cataloguemovie.ui.upcoming;

import com.ihsan.a49.cataloguemovie.model.ModelData;

import java.util.List;

public class UpcomingView {

    interface View {
        void setViewUpcomingMovie(List<ModelData> model);
    }


    interface Presenter {
        void loadDataUpcoming();

        void start();
    }
}
