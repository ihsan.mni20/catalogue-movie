package com.ihsan.a49.cataloguemovie.ui.playing;

import com.ihsan.a49.cataloguemovie.model.ModelData;

import java.util.List;

public class NowPlayingView {
    interface View {
        void setViewNowPlayingMovie(List<ModelData> model);
    }


    interface Presenter {
        void loadDataNowPlaying();

        void start();
    }

}
