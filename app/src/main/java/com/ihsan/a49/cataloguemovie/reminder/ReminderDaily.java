package com.ihsan.a49.cataloguemovie.reminder;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.ihsan.a49.cataloguemovie.R;
import com.ihsan.a49.cataloguemovie.ui.MainActivity;

import java.util.Calendar;

import static android.app.PendingIntent.FLAG_ONE_SHOT;

public class ReminderDaily extends BroadcastReceiver {

    public static final String EXTRA_MESSAGE = "message";

    @Override
    public void onReceive(Context context, Intent intent) {
        //Log.d("RETRO", "onReceive");
        String message = intent.getStringExtra(EXTRA_MESSAGE);

        showNotification(context, MainActivity.class, message);
    }


    public static void showNotification(Context context, Class<?> cls, String content) {
        Log.d("RETRO", "show notification ::" + ":::" + content);

        Intent notifyIntent = new Intent(context, cls);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

        PendingIntent pendingIntent = PendingIntent.getActivity(
                context,
                0,
                notifyIntent,
                FLAG_ONE_SHOT);

        builder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(content)
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
                .setContentInfo("Info");

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.notify(1, builder.build());

    }

    public static void setReminder(Context context, String message, int hour, int min) {
        // Log.d("RETRO", "set reminder");
        Calendar setCalendar = Calendar.getInstance();

        Intent intent = new Intent(context, ReminderDaily.class);
        intent.putExtra(EXTRA_MESSAGE, message);

        setCalendar.set(Calendar.HOUR_OF_DAY, hour);
        setCalendar.set(Calendar.MINUTE, min);
        setCalendar.set(Calendar.SECOND, 0);

        ComponentName recever = new ComponentName(context, ReminderDaily.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(recever, PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);


        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        assert am != null;
        am.setInexactRepeating(AlarmManager.RTC_WAKEUP, setCalendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);

    }

    public static void cancelReminder(Context context) {

        ComponentName receiver = new ComponentName(context, ReminderDaily.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);

        Intent intent = new Intent(context, ReminderDaily.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        assert am != null;
        am.cancel(pendingIntent);
        pendingIntent.cancel();

    }


}
