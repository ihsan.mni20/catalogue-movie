package com.ihsan.a49.cataloguemovie.ui;


import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.ihsan.a49.cataloguemovie.R;
import com.ihsan.a49.cataloguemovie.settings.SettingsActivity;
import com.ihsan.a49.cataloguemovie.ui.favorite.FavoriteFragment;
import com.ihsan.a49.cataloguemovie.ui.playing.NowPlayingFragment;
import com.ihsan.a49.cataloguemovie.ui.search.SearchActivity;
import com.ihsan.a49.cataloguemovie.ui.upcoming.UpcomingFragment;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity {

    @BindView(R.id.search_view)
    MaterialSearchView searchView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;

    MenuItem mSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        //startAlarm(true,true);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        Objects.requireNonNull(getSupportActionBar()).setElevation(0);

        tabLayout.setSmoothScrollingEnabled(true);
        tabLayout.setFillViewport(true);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
                intent.putExtra("search", query);
                startActivity(intent);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }


    private void setupViewPager(ViewPager viewPager) {
        String tabUpcoming = getResources().getString(R.string.tab_upcoming);
        String tabNowPlaying = getResources().getString(R.string.tab_now_playing);
        String tabFavorite = getResources().getString(R.string.tab_favorite);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new UpcomingFragment(), tabUpcoming);
        adapter.addFragment(new NowPlayingFragment(), tabNowPlaying);
        adapter.addFragment(new FavoriteFragment(), tabFavorite);
        viewPager.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.search_menu, menu);
        mSearch = menu.findItem(R.id.action_search);
        searchView.setMenuItem(mSearch);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_setting) {
            Intent mItem = new Intent(this, SettingsActivity.class);
            startActivity(mItem);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
