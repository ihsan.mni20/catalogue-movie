package com.ihsan.a49.cataloguemovie.ui.upcoming;

import android.annotation.SuppressLint;
import android.util.Log;

import com.ihsan.a49.cataloguemovie.data.api.ApiRequestData;
import com.ihsan.a49.cataloguemovie.data.api.MovieServer;
import com.ihsan.a49.cataloguemovie.model.ModelResponse;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class UpcomingPresenter implements UpcomingView.Presenter {

    private final UpcomingView.View view;

    public UpcomingPresenter(UpcomingView.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        loadDataUpcoming();
    }

    @SuppressLint("CheckResult")
    @Override
    public void loadDataUpcoming() {
        ApiRequestData api = MovieServer.getClient();
        Observable<ModelResponse> getData;

        getData = api.getUpComing();

        getData.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(movies -> view.setViewUpcomingMovie(movies.getResults()), it -> Log.d("Load Server ", it.toString()));
    }
}


