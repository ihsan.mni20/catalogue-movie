package com.ihsan.a49.cataloguemovie.ui.playing;

import android.annotation.SuppressLint;
import android.util.Log;

import com.ihsan.a49.cataloguemovie.data.api.ApiRequestData;
import com.ihsan.a49.cataloguemovie.data.api.MovieServer;
import com.ihsan.a49.cataloguemovie.model.ModelResponse;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class NowPlayingPresenter implements NowPlayingView.Presenter {

    private final NowPlayingView.View view;

    NowPlayingPresenter(NowPlayingView.View view) {
        this.view = view;
    }

    @SuppressLint("CheckResult")
    @Override
    public void loadDataNowPlaying() {
        ApiRequestData api = MovieServer.getClient();
        Observable<ModelResponse> getData;

        getData = api.getNowPlaying();

        getData.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(movies -> view.setViewNowPlayingMovie(movies.getResults()), it -> Log.d("Load Server ", it.toString()));
    }

    @Override
    public void start() {
        loadDataNowPlaying();
    }


}
