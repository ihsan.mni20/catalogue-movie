package com.ihsan.a49.cataloguemovie.ui.favorite;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ihsan.a49.cataloguemovie.BuildConfig;
import com.ihsan.a49.cataloguemovie.R;
import com.ihsan.a49.cataloguemovie.model.ModelDetail;
import com.ihsan.a49.cataloguemovie.ui.detail.DetailMovie;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ihsan.a49.cataloguemovie.data.database.DatabaseContract.CONTENT_URI;


public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.HolderData> {

    private static final String KEY_MOVIE = "idMovie";
    private final Activity activity;
    private List<ModelDetail> listFavorite;

    public FavoriteAdapter(Activity activity) {
        this.activity = activity;
    }

    private List<ModelDetail> getListFavorites() {
        return listFavorite;
    }

    public void setListFavorite(List<ModelDetail> listFavorite) {
        this.listFavorite = listFavorite;
    }

    @Override
    public void onBindViewHolder(@NonNull final HolderData holder, int position) {
        final ModelDetail md = listFavorite.get(position);

        holder.title.setText(md.getTitle());
        holder.release.setText(md.getRelease_date());
        Glide.with(holder.itemView.getContext())
                .load(BuildConfig.IMAGE_URL + md.getPoster_path())
                .into(holder.poster);
        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), DetailMovie.class);
            Uri uri = Uri.parse(CONTENT_URI + "/" + md.getId());
            intent.setData(uri);
            intent.putExtra(KEY_MOVIE, md.getId());
            v.getContext().startActivity(intent);
        });
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list, parent, false);
        return new HolderData(layout);
    }

    @Override
    public int getItemCount() {
        if (listFavorite == null) return 0;
        return getListFavorites().size();
    }

    static class HolderData extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        TextView title;
        @BindView(R.id.tv_release)
        TextView release;
        @BindView(R.id.img_poster)
        ImageView poster;


        HolderData(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }
}


