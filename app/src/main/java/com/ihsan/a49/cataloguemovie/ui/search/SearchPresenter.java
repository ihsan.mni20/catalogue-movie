package com.ihsan.a49.cataloguemovie.ui.search;

import android.annotation.SuppressLint;
import android.util.Log;

import com.ihsan.a49.cataloguemovie.data.api.ApiRequestData;
import com.ihsan.a49.cataloguemovie.data.api.MovieServer;
import com.ihsan.a49.cataloguemovie.model.ModelResponse;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SearchPresenter implements SearchView.Presenter {
    private final SearchView.View view;
    private final String query;

    public SearchPresenter(SearchView.View view, String query) {
        this.view = view;
        this.query = query;
    }

    @Override
    public void start() {
        loadDataSearch();
    }

    @SuppressLint("CheckResult")
    @Override
    public void loadDataSearch() {
        ApiRequestData api = MovieServer.getClient();
        Observable<ModelResponse> getData;

        getData = api.getSearch(query);

        getData.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(movies -> view.setViewSearchMovie(movies.getResults()), it -> Log.d("Load Server ", it.toString()));
    }
}
