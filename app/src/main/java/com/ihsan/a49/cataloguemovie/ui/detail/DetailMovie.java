package com.ihsan.a49.cataloguemovie.ui.detail;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ihsan.a49.cataloguemovie.BuildConfig;
import com.ihsan.a49.cataloguemovie.R;
import com.ihsan.a49.cataloguemovie.data.database.FavoriteHelper;
import com.ihsan.a49.cataloguemovie.model.ModelDetail;
import com.ihsan.a49.cataloguemovie.util.FormatDate;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ihsan.a49.cataloguemovie.data.database.DatabaseContract.CONTENT_URI;

public class DetailMovie extends AppCompatActivity implements DetailView.View {

    @BindView(R.id.detail_img_poster)
    ImageView detailPoster;
    @BindView(R.id.detail_title)
    TextView detailTitle;
    @BindView(R.id.detail_release_date)
    TextView detailRelease;
    @BindView(R.id.detail_genre)
    TextView detailGenre;
    @BindView(R.id.detail_vote_average)
    TextView detailVote;
    @BindView(R.id.detail_overview)
    TextView detailOverview;
    private static final String STATE_ITEMS = "items";
    private static final String KEY_MOVIE = "idMovie";
    private final StringBuilder genre = new StringBuilder();
    @BindView(R.id.iv_favorite)
    FloatingActionButton ivFavorite;
    @BindView(R.id.rating_bar)
    RatingBar ratingBar;
    @BindView(R.id.detail_img)
    ImageView backdrop;
    @BindView(R.id.tv_runtime)
    TextView runtime;


    public FavoriteHelper favoriteHelper;
    private boolean isFavorite = false;
    @BindView(R.id.vote_count)
    TextView voteCount;
    @BindView(R.id.content_overview)
    LinearLayout detailView;
    @BindView(R.id.loading)
    ProgressBar progressBar;
    private DetailPresenter detailPresenter;
    private ModelDetail mItems;
    private Uri uriDbMovie;
    private String idMovie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        Intent intent = getIntent();

        uriDbMovie = getIntent().getData();

        if (intent.getExtras() != null) {
            idMovie = (String) intent.getExtras().get(KEY_MOVIE);
        }

        detailView.setVisibility(View.GONE);
        ivFavorite.setVisibility(View.GONE);
        detailPresenter = new DetailPresenter(this, getBaseContext(), idMovie, uriDbMovie);

        favoriteHelper = new FavoriteHelper(this);
        favoriteHelper.open();

        uriDbMovie = Uri.parse(CONTENT_URI + "/" + idMovie);

        isFavorite = favoriteHelper.checkFavorite(idMovie);
        setFavoriteIcon(isFavorite);

        if (savedInstanceState != null) {

            mItems = savedInstanceState.getParcelable(STATE_ITEMS);
            this.setViewDetailMovie(Objects.requireNonNull(mItems));

        } else {

            detailPresenter.start();
        }

        ivFavorite.setOnClickListener(v -> {
            if (isFavorite) {
                detailPresenter.FavoriteDelete(uriDbMovie);
                onBackPressed();

            } else {
                detailPresenter.FavoriteSave(mItems);
            }
            isFavorite = !isFavorite;
            setFavoriteIcon(isFavorite);

        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (favoriteHelper != null) {
            favoriteHelper.close();
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(STATE_ITEMS, mItems);
        super.onSaveInstanceState(outState);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void setViewDetailMovie(ModelDetail model) {
        progressBar.setVisibility(View.GONE);

        mItems = model;

        float rate = Float.parseFloat(model.getVote_average()) / 2;
        detailTitle.setText(model.getTitle());
        detailVote.setText(Float.toString(rate));
        detailOverview.setText(model.getOverview());
        voteCount.setText(model.getVote_count());
        runtime.setText(model.getRuntime() + " Minutes");


        Glide.with(this)
                .load(BuildConfig.IMAGE_URL + model.getPoster_path()).error(R.drawable.ic_error)
                .into(detailPoster);
        Glide.with(this)
                .load(BuildConfig.IMAGE_URL + model.getBackdrop_path())
                .fallback(R.drawable.ic_error)
                .error(R.drawable.ic_error)
                .into(backdrop);


        ratingBar.setRating(Float.parseFloat(model.getVote_average()) / 2);
        if (isFavorite) {
            detailGenre.setText(model.getStringGenre());
            detailRelease.setText(model.getRelease_date());
        } else {

            String DateTime = FormatDate.loadDate(model.getRelease_date());
            model.setRelease_date(DateTime);
            detailRelease.setText(DateTime);

            int size = model.getGenres().size();

            for (int i = 0; i < size; i++) {

                if (i == size - 1) {
                    genre.append(model.getGenres().get(i).getName());
                } else {
                    genre.append(model.getGenres().get(i).getName()).append(", ");
                }
            }

            model.setStringGenre(genre.toString());
            detailGenre.setText(model.getStringGenre());

        }

        detailView.setVisibility(View.VISIBLE);
        ivFavorite.setVisibility(View.VISIBLE);

    }

    @Override
    public void setFavoriteIcon(boolean isFav) {
        if (isFav) {
            ivFavorite.setImageResource(R.drawable.ic_favorite);
            isFavorite = true;

        } else {
            ivFavorite.setImageResource(R.drawable.ic_favorite_border);
            isFavorite = false;
        }
    }


}
