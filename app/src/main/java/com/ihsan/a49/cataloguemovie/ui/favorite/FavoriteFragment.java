package com.ihsan.a49.cataloguemovie.ui.favorite;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ihsan.a49.cataloguemovie.R;
import com.ihsan.a49.cataloguemovie.model.ModelDetail;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class FavoriteFragment extends Fragment implements FavoriteView.View {
    @BindView(R.id.recycle_favorite)
    RecyclerView mRecycler;
    private static final String STATE_ITEMS = "items";
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    FavoriteAdapter favoriteAdapter;
    @BindView(R.id.tv_favorite)
    TextView textView;
    ArrayList<ModelDetail> mItems;
    FavoritePresenter favoritePresenter;
    private Unbinder unbinder;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.fragment_favorite, container, false);

        unbinder = ButterKnife.bind(this, V);
        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

        mRecycler.setHasFixedSize(true);

        favoritePresenter = new FavoritePresenter(this, this.getContext());

        return V;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            progressBar.setVisibility(View.GONE);
            textView.setVisibility(View.GONE);

            mItems = savedInstanceState.getParcelableArrayList(STATE_ITEMS);

            favoriteAdapter = new FavoriteAdapter(this.getActivity());
            favoriteAdapter.setListFavorite(mItems);
            mRecycler.setAdapter(favoriteAdapter);
            favoriteAdapter.notifyDataSetChanged();


        } else {
            progressBar.setVisibility(View.GONE);
            textView.setVisibility(View.GONE);

            favoritePresenter.start();
        }


    }

    @Override
    public void onResume() {
        favoritePresenter.start();
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelableArrayList(STATE_ITEMS, mItems);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void setViewFavoriteMovie(List<ModelDetail> model) {
        mItems = (ArrayList<ModelDetail>) model;
        favoriteAdapter = new FavoriteAdapter(this.getActivity());
        favoriteAdapter.setListFavorite(model);
        mRecycler.setAdapter(favoriteAdapter);
        favoriteAdapter.notifyDataSetChanged();
    }

}
