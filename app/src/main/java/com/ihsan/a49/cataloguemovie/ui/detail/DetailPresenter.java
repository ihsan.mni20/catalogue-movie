package com.ihsan.a49.cataloguemovie.ui.detail;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.ihsan.a49.cataloguemovie.data.api.ApiRequestData;
import com.ihsan.a49.cataloguemovie.data.api.MovieServer;
import com.ihsan.a49.cataloguemovie.model.ModelDetail;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static android.provider.BaseColumns._ID;
import static com.ihsan.a49.cataloguemovie.data.database.DatabaseContract.CONTENT_URI;
import static com.ihsan.a49.cataloguemovie.data.database.DatabaseContract.MovieColumns.BACKDROP_PATH;
import static com.ihsan.a49.cataloguemovie.data.database.DatabaseContract.MovieColumns.GENRE;
import static com.ihsan.a49.cataloguemovie.data.database.DatabaseContract.MovieColumns.OVERVIEW;
import static com.ihsan.a49.cataloguemovie.data.database.DatabaseContract.MovieColumns.POSTER_PATH;
import static com.ihsan.a49.cataloguemovie.data.database.DatabaseContract.MovieColumns.RELEASE_DATE;
import static com.ihsan.a49.cataloguemovie.data.database.DatabaseContract.MovieColumns.RUNTIME;
import static com.ihsan.a49.cataloguemovie.data.database.DatabaseContract.MovieColumns.TITLE;
import static com.ihsan.a49.cataloguemovie.data.database.DatabaseContract.MovieColumns.VOTE_AVERAGE;
import static com.ihsan.a49.cataloguemovie.data.database.DatabaseContract.MovieColumns.VOTE_COUNT;

public class DetailPresenter implements DetailView.Presenter {

    final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final DetailView.View view;
    private final Uri uri;
    private final Context mContext;
    private final String idMovie;
    private ModelDetail movies;

    DetailPresenter(DetailView.View view, Context mContext, String idMovie, Uri uri) {
        this.view = view;
        this.mContext = mContext;
        this.idMovie = idMovie;
        this.uri = uri;
    }


    public void FavoriteSave(ModelDetail movies) {

        ContentValues values = new ContentValues();

        values.put(_ID, movies.getId());
        values.put(TITLE, movies.getTitle());
        values.put(RELEASE_DATE, movies.getRelease_date());
        values.put(POSTER_PATH, movies.getPoster_path());
        values.put(OVERVIEW, movies.getOverview());
        values.put(VOTE_AVERAGE, movies.getVote_average());
        values.put(BACKDROP_PATH, movies.getBackdrop_path());
        values.put(VOTE_COUNT, movies.getVote_count());
        values.put(RUNTIME, movies.getRuntime());
        values.put(GENRE, movies.getStringGenre());

        Completable favSave = Completable.create(e -> {
            mContext.getContentResolver().insert(CONTENT_URI, values);

            e.onComplete();
        });

        compositeDisposable.add(favSave
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> Toast.makeText(mContext, "Save Favorite", Toast.LENGTH_SHORT).show(), throwable -> Log.d("error", String.valueOf(throwable))));
    }

    @SuppressLint({"ShowToast", "CheckResult"})
    public void FavoriteDelete(Uri uri) {

        Completable favDelete = Completable.create(e -> {
            mContext.getContentResolver().delete(uri, null, null);
            e.onComplete();
        });

        compositeDisposable.add(favDelete
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> Toast.makeText(mContext, "Delete Favorite", Toast.LENGTH_SHORT).show(), throwable -> Log.d("error", String.valueOf(throwable))));

    }


    @SuppressLint("CheckResult")
    @Override
    public void loadDataDetail() {

        ApiRequestData api = MovieServer.getClient();
        Observable<ModelDetail> getData;

        getData = api.getDetail(idMovie);
        getData.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::setViewDetailMovie, it -> Log.d("Load Server ", it.toString()));
    }

    @SuppressLint("CheckResult")
    @Override
    public void loadDataDetailFav() {
        if (uri != null) {
            Cursor cursor = mContext.getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    movies = new ModelDetail(cursor);
                    Log.d("fav detail", movies.getTitle());
                    view.setViewDetailMovie(movies);
                }
                cursor.close();
            }
        }
    }

    @Override
    public void start() {
        if (uri != null) {
            loadDataDetailFav();
        } else {
            loadDataDetail();
        }
    }
}
