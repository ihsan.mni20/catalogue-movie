package com.ihsan.a49.cataloguemovie.model;

import java.util.List;

public class ModelResponse {

    private final List<ModelData> results;

    public ModelResponse(List<ModelData> results) {
        this.results = results;
    }

    public List<ModelData> getResults() {
        return results;
    }

}
