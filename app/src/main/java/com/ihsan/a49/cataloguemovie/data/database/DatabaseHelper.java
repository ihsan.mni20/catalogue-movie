package com.ihsan.a49.cataloguemovie.data.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ihsan.a49.cataloguemovie.data.database.DatabaseContract.MovieColumns;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "dbmovie";
    private static final int DATABASE_VERSION = 1;
    private static final String SQL_CREATE_TABLE_MOVIE = "CREATE TABLE " + DatabaseContract.TABLE_FAVORITE +
            "(" + MovieColumns._ID + " TEXT PRIMARY KEY ," +
            MovieColumns.TITLE + " TEXT NOT NULL," +
            MovieColumns.OVERVIEW + " TEXT NOT NULL," +
            MovieColumns.RELEASE_DATE + " TEXT NOT NULL," +
            MovieColumns.VOTE_AVERAGE + " TEXT NOT NULL," +
            MovieColumns.POSTER_PATH + " TEXT," +
            MovieColumns.BACKDROP_PATH + " TEXT," +
            MovieColumns.VOTE_COUNT + " TEXT NOT NULL," +
            MovieColumns.RUNTIME + " TEXT NOT NULL," +
            MovieColumns.GENRE + " TEXT NOT NULL)";


    DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_MOVIE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.TABLE_FAVORITE);
        onCreate(db);
    }
}
