package com.ihsan.a49.cataloguemovie.data.api;

import androidx.annotation.NonNull;

import com.ihsan.a49.cataloguemovie.BuildConfig;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ApiParameterInterceptor implements Interceptor {
    @NonNull
    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        //getData = api.getDetail(idMovie, BuildConfig.API_MOVIEDB, Locale.getDefault().getCountry().toLowerCase());

        final HttpUrl url = chain.request()
                .url()
                .newBuilder()
                .addQueryParameter("api_key", BuildConfig.API_MOVIEDB)
                .addQueryParameter("language", "en-US")
                .build();

        final Request request = chain.request().newBuilder().url(url).build();
        return chain.proceed(request);
    }
}
