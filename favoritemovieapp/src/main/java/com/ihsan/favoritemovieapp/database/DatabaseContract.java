package com.ihsan.favoritemovieapp.database;


import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseContract {

    private static final String TABLE_FAVORITE = "favorite";

    public static final class MovieColumns implements BaseColumns {

        public static final String TITLE = "title";
        public static final String POSTER_PATH = "poster_path";
        public static final String OVERVIEW = "overview";
        public static final String RELEASE_DATE = "release_date";
        public static final String VOTE_AVERAGE = "vote_average";
        public static final String BACKDROP_PATH = "backdrop_path";
        public static final String VOTE_COUNT = "vote_count";
        public static final String RUNTIME = "runtime";
        public static final String GENRE = "genre";
    }

    private static final String AUTHORITY = "com.ihsan.a49.cataloguemovie";

    public static final Uri CONTENT_URI = new Uri.Builder().scheme("content")
            .authority(AUTHORITY)
            .appendPath(TABLE_FAVORITE)
            .build();

    public static String getColumnString(Cursor cursor, String columnName) {
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

}
