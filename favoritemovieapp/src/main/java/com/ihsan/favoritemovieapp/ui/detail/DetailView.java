package com.ihsan.favoritemovieapp.ui.detail;

import android.database.Cursor;

import com.ihsan.favoritemovieapp.model.MovieItem;

import io.reactivex.Observable;

public class DetailView {
    interface View {
        void setViewDetailMovie(MovieItem model);
    }


    interface Presenter {
        void loadDataDetail();

        Observable<Cursor> fetchDetailMovies();

        void start();
    }

}
