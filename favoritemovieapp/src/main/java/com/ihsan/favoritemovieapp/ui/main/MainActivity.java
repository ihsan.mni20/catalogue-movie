package com.ihsan.favoritemovieapp.ui.main;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ihsan.favoritemovieapp.R;
import com.ihsan.favoritemovieapp.adapter.favorite.FavoriteAdapter;
import com.ihsan.favoritemovieapp.model.MovieItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainView.View {

    private static final String STATE_ITEMS = "items";
    @BindView(R.id.recycle_favorite)
    RecyclerView lvFavorite;
    ArrayList<MovieItem> mItems;
    FavoriteAdapter favoriteAdapter;
    MainPresenter favoritePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        lvFavorite.setLayoutManager(new LinearLayoutManager(this));
        lvFavorite.setHasFixedSize(true);

        favoriteAdapter = new FavoriteAdapter(this);
        favoritePresenter = new MainPresenter(this, this);
        if (savedInstanceState != null) {

            mItems = savedInstanceState.getParcelableArrayList(STATE_ITEMS);

            favoriteAdapter.setListFavorite(mItems);
            lvFavorite.setAdapter(favoriteAdapter);
            favoriteAdapter.notifyDataSetChanged();

        } else {

            favoritePresenter.start();
        }
    }


    @Override
    public void setViewFavoriteMovie(List<MovieItem> model) {
        mItems = (ArrayList<MovieItem>) model;
        favoriteAdapter = new FavoriteAdapter(this);
        favoriteAdapter.setListFavorite(model);
        lvFavorite.setAdapter(favoriteAdapter);
        favoriteAdapter.notifyDataSetChanged();
    }

    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelableArrayList(STATE_ITEMS, mItems);
        super.onSaveInstanceState(outState);
    }
}
