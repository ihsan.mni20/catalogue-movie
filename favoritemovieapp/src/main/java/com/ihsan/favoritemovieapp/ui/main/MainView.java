package com.ihsan.favoritemovieapp.ui.main;

import com.ihsan.favoritemovieapp.model.MovieItem;

import java.util.List;

import io.reactivex.Observable;

public class MainView {
    interface View {
        void setViewFavoriteMovie(List<MovieItem> model);
    }


    interface Presenter {
        void loadDataFavorite();

        Observable<List<MovieItem>> fetchFavoriteMovies();

        void start();
    }
}
