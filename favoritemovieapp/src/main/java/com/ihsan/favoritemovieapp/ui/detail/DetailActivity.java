package com.ihsan.favoritemovieapp.ui.detail;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ihsan.favoritemovieapp.R;
import com.ihsan.favoritemovieapp.model.MovieItem;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends AppCompatActivity implements DetailView.View {
    private static final String STATE_ITEMS = "items";
    @BindView(R.id.detail_img_poster)
    ImageView detailPoster;
    @BindView(R.id.detail_title)
    TextView detailTitle;
    @BindView(R.id.detail_release_date)
    TextView detailRelease;
    @BindView(R.id.detail_genre)
    TextView detailGenre;
    @BindView(R.id.detail_vote_average)
    TextView detailVote;
    @BindView(R.id.detail_overview)
    TextView detailOverview;
    @BindView(R.id.rating_bar)
    RatingBar ratingBar;
    @BindView(R.id.detail_img)
    ImageView backdrop;
    @BindView(R.id.tv_runtime)
    TextView runtime;
    @BindView(R.id.vote_count)
    TextView voteCount;
    @BindView(R.id.content_overview)
    LinearLayout detailView;
    @BindView(R.id.loading)
    ProgressBar progressBar;
    private DetailPresenter detailPresenter;
    private MovieItem mItems;
    private Uri uriDbMovie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        uriDbMovie = getIntent().getData();

        detailPresenter = new DetailPresenter(this, getBaseContext(), uriDbMovie);

        if (savedInstanceState != null) {
            mItems = savedInstanceState.getParcelable(STATE_ITEMS);
            this.setViewDetailMovie(Objects.requireNonNull(mItems));
        } else {
            detailPresenter.start();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void setViewDetailMovie(MovieItem model) {

        mItems = model;

        float rate = Float.parseFloat(model.getVote_average()) / 2;
        detailTitle.setText(model.getTitle());
        detailVote.setText(Float.toString(rate));
        detailOverview.setText(model.getOverview());
        voteCount.setText(model.getVote_count());
        runtime.setText(model.getRuntime() + getString(R.string.minutes));

        Picasso.get().load("http://image.tmdb.org/t/p/w185" + model.getPoster_path()).into(detailPoster);
        Picasso.get().load("http://image.tmdb.org/t/p/w185" + model.getBackdrop_path()).into(backdrop);

        String DateTime = model.getRelease_date();
        detailRelease.setText(DateTime);

        ratingBar.setRating(Float.parseFloat(model.getVote_average()) / 2);
        detailGenre.setText(model.getStringGenre());
        detailRelease.setText(model.getRelease_date());
        detailGenre.setText(model.getStringGenre());

    }

}
