package com.ihsan.favoritemovieapp.ui.detail;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.ihsan.favoritemovieapp.model.MovieItem;

import java.util.Objects;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class DetailPresenter implements DetailView.Presenter {
    final Uri uri;
    private final DetailView.View view;
    private final Context mContext;

    DetailPresenter(DetailView.View view, Context mContext, Uri uri) {
        this.view = view;
        this.uri = uri;
        this.mContext = mContext;
    }

    @Override
    public void loadDataDetail() {
        Observable<Cursor> observableMovies = fetchDetailMovies();
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(observableMovies.
                subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(movies -> {
                    MovieItem movieItem = new MovieItem();
                    if (movies.moveToFirst()) movieItem = new MovieItem(movies);

                    view.setViewDetailMovie(movieItem);
                    movies.close();
                    Log.d("fav", movieItem.getTitle());

                }, throwable -> Log.e("error", Objects.requireNonNull(throwable.getLocalizedMessage()))));
    }

    @Override
    public Observable<Cursor> fetchDetailMovies() {
        return Observable.create(e -> {
            Cursor cursor = mContext.getContentResolver().query(uri, null, null, null, null);

            e.onNext(cursor);
        });
    }

    @Override
    public void start() {
        loadDataDetail();

    }
}
