package com.ihsan.favoritemovieapp.ui.main;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.ihsan.favoritemovieapp.model.MovieItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.ihsan.favoritemovieapp.database.DatabaseContract.CONTENT_URI;

public class MainPresenter implements MainView.Presenter {
    private final MainView.View view;
    private final Context mContext;

    MainPresenter(MainView.View view, Context mContext) {
        this.view = view;
        this.mContext = mContext;
    }


    @Override
    public void loadDataFavorite() {
        Observable<List<MovieItem>> observableMovies = fetchFavoriteMovies();
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(observableMovies.
                subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(movies -> {
                    view.setViewFavoriteMovie(movies);
                    Log.d("fav", movies.get(0).getTitle());

                }, throwable -> Log.e("error", throwable.getLocalizedMessage())));
    }


    @Override
    public void start() {
        loadDataFavorite();
    }

    @Override
    public Observable<List<MovieItem>> fetchFavoriteMovies() {
        return Observable.create(e -> {
            List<MovieItem> movies = new ArrayList<>();
            final Cursor cursor = mContext.getContentResolver().query(CONTENT_URI, null, null, null, null);

            if (Objects.requireNonNull(cursor).moveToFirst()) {
                do {
                    movies.add(new MovieItem(cursor));
                } while (cursor.moveToNext());
            }

            cursor.close();
            e.onNext(movies);
        });
    }

}
