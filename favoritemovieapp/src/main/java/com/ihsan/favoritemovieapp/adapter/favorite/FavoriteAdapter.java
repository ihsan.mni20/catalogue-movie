package com.ihsan.favoritemovieapp.adapter.favorite;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ihsan.favoritemovieapp.R;
import com.ihsan.favoritemovieapp.model.MovieItem;
import com.ihsan.favoritemovieapp.ui.detail.DetailActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ihsan.favoritemovieapp.database.DatabaseContract.CONTENT_URI;


public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.HolderData> {

    private final Activity activity;
    private List<MovieItem> listFavorite;

    public FavoriteAdapter(Activity activity) {
        this.activity = activity;
    }

    private List<MovieItem> getListFavorites() {
        return listFavorite;
    }

    public void setListFavorite(List<MovieItem> listFavorite) {
        this.listFavorite = listFavorite;
    }

    @Override
    public void onBindViewHolder(@NonNull final HolderData holder, int position) {
        final MovieItem md = listFavorite.get(position);

        holder.title.setText(md.getTitle());
        holder.release.setText(md.getRelease_date());
        Picasso.get().load("http://image.tmdb.org/t/p/w185" + md.getPoster_path()).into(holder.poster);

        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), DetailActivity.class);
            Uri uri = Uri.parse(CONTENT_URI + "/" + md.getId());
            intent.setData(uri);
            intent.putExtra("idMovie", md.getId());
            v.getContext().startActivity(intent);
        });
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list, parent, false);
        return new HolderData(layout);
    }

    @Override
    public int getItemCount() {
        if (listFavorite == null) return 0;
        return getListFavorites().size();
    }

    static class HolderData extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        TextView title;
        @BindView(R.id.tv_release)
        TextView release;
        @BindView(R.id.img_poster)
        ImageView poster;


        HolderData(View v) {

            super(v);
            ButterKnife.bind(this, v);
        }

    }

}


