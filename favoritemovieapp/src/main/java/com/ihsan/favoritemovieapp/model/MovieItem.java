package com.ihsan.favoritemovieapp.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.ihsan.favoritemovieapp.database.DatabaseContract;

import static android.provider.BaseColumns._ID;
import static com.ihsan.favoritemovieapp.database.DatabaseContract.getColumnString;

public class MovieItem implements Parcelable {


    public static final Creator<MovieItem> CREATOR = new Creator<MovieItem>() {
        @Override
        public MovieItem createFromParcel(Parcel in) {
            return new MovieItem(in);
        }

        @Override
        public MovieItem[] newArray(int size) {
            return new MovieItem[size];
        }
    };
    private String title, poster_path, overview, release_date, vote_average, id,
            backdrop_path, vote_count, runtime, stringGenre;

    public MovieItem() {

    }

    protected MovieItem(Parcel in) {
        title = in.readString();
        poster_path = in.readString();
        overview = in.readString();
        release_date = in.readString();
        vote_average = in.readString();
        id = in.readString();
        backdrop_path = in.readString();
        vote_count = in.readString();
        runtime = in.readString();
        stringGenre = in.readString();
    }

    public MovieItem(Cursor cursor) {
        this.title = getColumnString(cursor, DatabaseContract.MovieColumns.TITLE);
        this.poster_path = getColumnString(cursor, DatabaseContract.MovieColumns.POSTER_PATH);
        this.overview = getColumnString(cursor, DatabaseContract.MovieColumns.OVERVIEW);
        this.release_date = getColumnString(cursor, DatabaseContract.MovieColumns.RELEASE_DATE);
        this.vote_average = getColumnString(cursor, DatabaseContract.MovieColumns.VOTE_AVERAGE);
        this.backdrop_path = getColumnString(cursor, DatabaseContract.MovieColumns.BACKDROP_PATH);
        this.vote_count = getColumnString(cursor, DatabaseContract.MovieColumns.VOTE_COUNT);
        this.runtime = getColumnString(cursor, DatabaseContract.MovieColumns.RUNTIME);
        this.stringGenre = getColumnString(cursor, DatabaseContract.MovieColumns.GENRE);
        this.id = getColumnString(cursor, _ID);
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public String getVote_count() {
        return vote_count;
    }

    public void setVote_count(String vote_count) {
        this.vote_count = vote_count;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getStringGenre() {
        return stringGenre;
    }

    public void setStringGenre(String stringGenre) {
        this.stringGenre = stringGenre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getVote_average() {
        return vote_average;
    }

    public void setVote_average(String vote_average) {
        this.vote_average = vote_average;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(poster_path);
        parcel.writeString(overview);
        parcel.writeString(release_date);
        parcel.writeString(vote_average);
        parcel.writeString(id);
        parcel.writeString(backdrop_path);
        parcel.writeString(vote_count);
        parcel.writeString(runtime);
        parcel.writeString(stringGenre);
    }
}
